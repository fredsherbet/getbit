#!/usr/bin/python2

import logging
import random

log = logging.getLogger(__name__)


class GameState:
    """Public game state"""
    def __init__(self, num_cards):
        # ix 0 is furthest from the shark. Players referenced by name.
        self.robots = []
        self.dead_robots = []
        self.num_cards = num_cards

    @property
    def num_played_cards(self):
        return len(self.robots[0].played_cards)

    def add_robot(self, robot_name):
        log.debug("Adding game state robot %s", robot_name)
        self.robots.append(PublicRobot(robot_name, self.num_cards))

    def move_robot_to_front(self, robot_name):
        # This is slightly ugly! But internet doesn't have any better suggestions.
        # Remove the robot, and re-insert at the front.
        robot = self.get_robot(robot_name)
        ix = self.robots.index(robot)
        self.robots.pop(ix)
        self.robots.insert(0, robot)

    def get_robot(self, robot_name):
        log.debug("Looking for public robot %s", robot_name)
        for r in self.robots:
            if r.name == robot_name:
                return r

    def other_robots(self, robot_name):
        for r in self.robots:
            if r.name != str(robot_name):
                yield r

    def take_a_bite(self):
        """Returns the name of the robot if it is dead"""
        robot = self.robots[-1]
        robot.num_limbs -= 1
        log.warning("Robot %s loses a limb; %s left", robot, robot.num_limbs)
        if robot.num_limbs <= 0:
            log.warning("%s died!", robot)
            self.dead_robots.append(robot)
            self.robots = [r for r in self.robots if r != robot]
            return robot.name

    def __str__(self):
        s = "=== GAME STATE ===\n"
        s += ", ".join("{0} ({1})".format(r.name, r.num_limbs) for r in self.robots)
        s += ", SHARK"
        if self.dead_robots:
            s += "\nDead robots: " + ", ".join(r.name for r in self.dead_robots)
        return s


class GetBit:
    """The game - encapsulates all the rules of the game and player interactions"""
    def __init__(self, num_players=4):
        self.robots = []
        # Min players is 4
        assert num_players >= 4
        self.num_players = num_players
        self.game_state = GameState(self.num_cards)
        self.colours = ["red", "purple", "blue", "green", "orange", "yellow", "white"]

    @property
    def num_cards(self):
        return self.num_players + 1

    def add_robot(self, robotClass):
        colour = self.colours.pop()
        robot = robotClass(self.num_cards, colour)
        self.game_state.add_robot(robot.name)
        self.robots.append(robot)

    def play(self):
        while len(self.robots) != self.num_players:
            self.add_robot(Robot)
        # Before play starts proper, the positions are randomized, and after
        # the very first round has no lost limbs, to set up a starting order.
        random.shuffle(self.robots)
        self.play_round(lose_limb=False)
        while not self.finished:
            self.play_round()
            log.info(self.game_state)
        log.warning("WINNER: %s", self.winner)
        print self.winner

    def play_round(self, lose_limb=True):
        log.info("=== NEW ROUND ===")
        robot_plays = self._play_cards()
        log.info("All cards played; revealing")
        unique_cards = self._reveal_played_cards(robot_plays)
        log.info("All cards played; moving forward")
        self._rearrange_robots(unique_cards)
        if lose_limb:
            robot_name = self.game_state.take_a_bite()
            # Remove dead robots from play :(
            self.robots = [r for r in self.robots if r.name != robot_name]
        if self.num_cards - self.game_state.num_played_cards <= 1:
            self._reset_cards()

    @property
    def finished(self):
        return len(self.robots) <= 2

    @property
    def winner(self):
        if self.finished:
            return self.robots[0].name
        return "Not finished yet!"

    @property
    def dead_robots(self):
        return self.game_state.dead_robots

    def _play_cards(self):
        robot_plays = {}
        for r in self.robots:
            robot_plays[r] = r.take_turn(self.game_state)
            log.info("Robot %s taking a turn", r)
            log.debug("Played card %s", robot_plays[r])
        return robot_plays

    def _reveal_played_cards(self, robot_plays):
        """robot_plays is a dictionary of the robots and their played card"""
        bad_cards = set()
        unique_cards = {}
        for robot, card in robot_plays.iteritems():
            log.info("Robot %s has played card %s", robot, card)
            self.game_state.get_robot(robot.name).played_cards.add(card)
            if card in bad_cards:
                log.info("But that card has already been cancelled out")
                continue
            if card in unique_cards:
                log.info("That's already been played, so cancelling out")
                bad_cards.add(card)
                del unique_cards[card]
                continue
            log.info("That's the first time that card's been played in this turn :)")
            unique_cards[card] = robot
        return unique_cards

    def _rearrange_robots(self, cards):
        """cards is dictionary of the cards played, with robot that played the card"""
        for c in range(1, self.num_cards + 1):
            if c in cards:
                log.warning("Robot %s played card %s; moving to front", cards[c], c)
                self._move_robot_to_front(cards[c])
                self.game_state.move_robot_to_front(str(cards[c]))

    def _move_robot_to_front(self, robot):
        # This is slightly ugly! But internet doesn't have any better suggestions.
        # Remove the robot, and re-insert at the front.
        ix = self.robots.index(robot)
        self.robots.pop(ix)
        self.robots.insert(0, robot)

    def _reset_cards(self):
        for robot in self.robots:
            robot.played_cards = set()
        for robot in self.game_state.robots:
            robot.played_cards = set()


class PublicRobot:
    def __init__(self, name, num_cards):
        self.name = name
        self.played_cards = set()
        self.num_limbs = 4
        self.num_cards = num_cards

    def __str__(self):
        return self.name

    def __eq__(self, other):
        return isinstance(other, PublicRobot) and other.name == self.name

    def __hash__(self):
        return hash(str(self))

    @property
    def unplayed_cards(self):
        return [c for c in xrange(1, self.num_cards + 1) if c not in self.played_cards]

    @property
    def available_cards(self):
        return self.unplayed_cards


class Robot(PublicRobot):
    """Base robot - write your own player by inheriting from this class and
    writing your own choose_card method"""

    PREFIX = "random"

    def __init__(self, num_cards, colour=None, name=None):
        if name:
            PublicRobot.__init__(self, name, num_cards)
        else:
            assert colour is not None
            PublicRobot.__init__(self, self.PREFIX + "_" + colour, num_cards)
        self.base_cards = set(range(1, num_cards + 1))

    @property
    def available_cards(self):
        return [c for c in self.base_cards if c not in self.played_cards]

    def choose_card(self, game_state):
        """Override this method with your algorithm. You have access to the following.

        * self.available_cards - Cards in your hand

        @TODO - Need to add the game state - order of players, and their played cards.
        """
        return random.choice(self.available_cards)

    def take_turn(self, game_state):
        chosen = self.choose_card(game_state)
        assert chosen in self.base_cards
        assert chosen not in self.played_cards
        self.played_cards.add(chosen)
        return chosen


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    game = GetBit()
    game.play()
