#!/usr/bin/python2

import logging
import getbit
from matt import MattRobot

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    game = getbit.GetBit()
    game.add_robot(MattRobot)
    game.play()
