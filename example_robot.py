#!/usr/bin/python

import logging
import getbit

log = logging.getLogger(__name__)


class YourRobot(getbit.Robot):
    PREFIX = "YourRobot"

    def choose_card(self, game_state):
        """Insert your implementation here... see README.md for more info"""
        return max(self.available_cards)

if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    game = getbit.GetBit()
    game.add_robot(YourRobot)
    game.play()
