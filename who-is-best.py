#!/usr/bin/python2

import sys
import getbit

results = {}
for attempt in xrange(int(sys.argv[1])):
    game = getbit.GetBit()
    game.play()
    if game.winner in results:
        results[game.winner] += 1
    else:
        results[game.winner] = 1
print results

