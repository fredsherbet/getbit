# Get Bit game

An implementation of the Get Bit game. Currently it is not interactive - all players must be computer algorithms.

## Implementing your own player

Write your own Robot class, inheriting from `Robot`, for example, see `example_robot.py`.

And change `choose_card` to be a great player! Just return the value of the card you want to play.

You have access to the following to make a wise decision.

* `self.available_cards` - the cards in your hand
* `self.num_cards` - The number of cards each player has (in total - i.e. this is constant for the game)
* `game_state.robots` - All the robots still in the game, and you can see the following about each.
    * `name`, which could be used to identify the algorithm - know your competitors!
    * `num_limbs` - How many limbs they have left
    * `played_cards` - Cards they don't have in their hand
    * `unplayed_cards` - Cards they have in their hand (which you could deduce from `played_cards`)
