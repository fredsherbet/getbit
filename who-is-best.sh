#!/bin/bash

if [[ -z $1 || -z $2 ]]
then
  echo "USAGE: $0 game attempts"
  exit 1
fi

game=$1
attempts=$2
attempt=0

while ((attempt < attempts))
do 
  python "$game" 2>/dev/null
  attempt=$((attempt + 1))
done | sort | uniq -c
